#include <iostream>

using namespace std;
int main()
{
    int n;
    cin >> n;
    if (n == 0)
    {
        cout << "you didn't enter a magic number!";
    }
    else if (n == 1)
    {
        cout<<"0";
    }
    else if (n == 2)
    {
        cout<<"0 1";
    }
    else
    {
        int a=0,b=1;
        cout<<"0 1 ";
        for(int i=0;i<n-2;i++){
            int c=a+b;
            a=b;
            b=c;
            cout<<c<<" ";
        }
    }
    return 0;
}