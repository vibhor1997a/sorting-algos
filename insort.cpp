#include <iostream>

using namespace std;


void printArr(int* arr,int size){
    cout<<"[";
    int i=0;
    for(i=0;i<size-1;i++){
        cout<<arr[i]<<", ";
    }
    cout<<arr[i]<<"]";
}
void swap(int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

int main()
{
    int n, t;
    cin >> n;
    t = n;
    int arr[30];
    int i = 0;
    while (t--)
    {
        cin >> arr[i];
        i++;
    }
    for (i = 1; i < n; i++)
    {
        for (int j = i; j > 0; j--)
        {
            if (arr[j - 1] > arr[j])
            {
                swap(&arr[j - 1], &arr[j]);
            }
        }
    }
    printArr(arr,n);
    return 0;
}