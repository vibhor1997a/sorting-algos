#include <iostream>

using namespace std;

void merge(int arr[], int l, int m, int r)
{
    int i, j, k;
    int n1 = m - l + 1;
    int n2 = r - m;

    /* create temp arrays */
    int L[n1], R[n2];

    /* Copy data to temp arrays L[] and R[] */
    for (i = 0; i < n1; i++)
        L[i] = arr[l + i];
    for (j = 0; j < n2; j++)
        R[j] = arr[m + 1 + j];

    /* Merge the temp arrays back into arr[l..r]*/
    i = 0; // Initial index of first subarray
    j = 0; // Initial index of second subarray
    k = l; // Initial index of merged subarray
    while (i < n1 && j < n2)
    {
        if (L[i] <= R[j])
        {
            arr[k] = L[i];
            i++;
        }
        else
        {
            arr[k] = R[j];
            j++;
        }
        k++;
    }
    }

    void mergeSort(int arr[], int l, int r)
    {
        if (l < r)
        {
            int m = (l + r - 1) / 2;
            mergeSort(arr, l, m);
            printArr(arr,m-l+1);
            mergeSort(arr, m + 1, r);
            merge(arr, l, m, r);
        }
    }

    void printArr(int *arr, int size)
    {
        cout << "[";
        int i = 0;
        for (i = 0; i < size - 1; i++)
        {
            cout << arr[i] << ", ";
        }
        cout << arr[i] << "]";
    }

    int main()
    {
        int n, t;
        cin >> n;
        t = n;
        int arr[n];
        int i = 0;
        while (t--)
        {
            cin >> arr[i];
            i++;
        }
        mergeSort(arr,0,n-1);
        printArr(arr,n);
        return 0;
    }