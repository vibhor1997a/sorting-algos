#include <iostream>

using namespace std;

void printArr(int *arr, int size)
{
    cout << "[";
    int i = 0;
    for (i = 0; i < size - 1; i++)
    {
        cout << arr[i] << ", ";
    }
    cout << arr[i] << "]";
}

void swap(int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

int partition(int arr[], int l, int r)
{
    int pivot = arr[r];
    int pi = l;
    for (int i = l; i < r; i++)
    {
        if (arr[i] <= pivot)
        {
            swap(&arr[i], &arr[pi]);
            pi++;
        }
    }
    swap(&arr[pi], &arr[r]);
    return pi;
}

void quicksort(int arr[], int l, int r)
{
    if (l < r)
    {
        int pi = partition(arr, l, r);
        quicksort(arr, l, pi - 1);
        quicksort(arr, pi + 1, r);
    }
}

int main()
{
    int n, t;
    cin >> n;
    t = n;
    int arr[30];
    int i = 0;
    while (t--)
    {
        cin >> arr[i];
        i++;
    }
    quicksort(arr, 0, n - 1);
    printArr(arr, n);
    return 0;
}